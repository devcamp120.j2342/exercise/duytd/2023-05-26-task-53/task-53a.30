import models.Employee;
public class App {
    public static void main(String[] args) throws Exception {
        Employee employee1 = new Employee(1,"Messi", "Leonel", 5000000);
        Employee employee2 = new Employee(2,"Ronaldo", "Cristiano", 15000000);

        System.out.println("employee 1");
        System.out.println(employee1.toString());
        System.out.println(employee1.getName());
        System.out.println(employee1.getAnulSalary());
        System.out.println(employee1.raiseSalary(10));
        System.out.println("employee 2");
        System.out.println(employee2.toString());
        System.out.println(employee2.getName());
        System.out.println(employee2.getAnulSalary());
        System.out.println(employee1.raiseSalary(10));
    }
}
